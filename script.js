
const http=require("http");
const port = 4000

const server= http.createServer((req, res)=>{
    // Route for returning all items upon receiving a GET request
    if(req.url=="/" && req.method == "GET"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.write('Welcome to Booking System');
        res.end();
    }else if(req.url=="/profile" && req.method == "GET"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.write('Welcome to your profile');
        res.end();
    }else if(req.url=="/courses" && req.method == "GET"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.write("Here's our courses avialable");
        res.end();
    }else if(req.url=="/addCourse" && req.method == "POST"){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.write("Add a course to our resources");
        res.end();
    }else {
        res.writeHead(404,{'Content-Type':'text/plain'});
        res.write('Page not Found');
        res.end();
    }
  
}).listen(port) 
console.log(`Server running at localhost: ${port}`);
